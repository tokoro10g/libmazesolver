#ifndef AGENT_H
#define AGENT_H

#include "maze.h"
#include "graph.h"
#include "wallsource.h"
//#include <cmath>

namespace MazeSolver{
	class Agent
	{
		public:
			Agent(){}
			Agent(Maze *_maze, WallSource *_wallSource,Graph *_graph):index(_maze->getWidth()-1),maze(_maze),wallSource(_wallSource),graph(_graph){
				dir.half=0x1; c.x=0; c.y=0; c.dir.half=0x1;
			}
			void reroute() { rerouteFlag=true; }
			bool searchMaze(int16_t start,int16_t goal,bool isSearching);
			void getShortestRoute(int16_t start, int16_t goal, Route& route);
			int stepMaze(int16_t goal,bool isSearching);
			int16_t searchCandidateNode(int16_t goal);
			Direction nextDir(int16_t from,int16_t to,bool isSearching);
			bool isPullBack(int16_t from,int16_t to,Direction dir) const;
			bool isPullBack(int16_t from,int16_t to,Direction dir,Direction newDir) const;
			bool isInGoalCell(int16_t index, int16_t goal) const;
			int16_t getIndex() const{ return index; }
			uint8_t getCurrentEdges(int16_t *edges) const;
			void setIndex(int16_t _index){ index=_index; }
			Coord getCoord() const{ return c; }
			Direction getDir() const{ return dir; }
			void setDir(const Direction _dir){ dir=_dir; }
			Direction getRelativeDir(Direction d) const;
			Route getRoute() const{ return route; }
			uint8_t getWidth() const { return maze->getWidth(); }
			uint8_t getHeight() const { return maze->getHeight(); }
			bool isReroute() const { return rerouteFlag; }
		private:
			bool senseFront() const;
			bool senseRight() const;
			bool senseLeft() const;
			void senseAll();
			bool sense(Direction newDir) const;
		private:
			int16_t index;
			Coord c;
			Direction dir;
			Direction sensorData;
			Route route;
			Maze *maze;
			WallSource *wallSource;
			Graph *graph;
			bool rerouteFlag;
	};
}

#endif // AGENT_H
