#include "graph.h"
//#include <queue>
#include <set>
//#include <utility>
//#include <algorithm>
//#include <iostream>
#include <cstdlib>

namespace MazeSolver{
	Node::~Node(){
		/* Todo:
		 * Edge$B$N4IM}$r$7$J$1$l$P$J$i$J$$(B */
	}

	int16_t Node::generateIndex(const Coord &c,Direction dir,uint8_t w){
		int16_t index=c.y*(2*w-1)+c.x;
		if(dir.half==0x1){
			return index+w-1;
		} else if(dir.half==0x2){
			return index;
		} else if(dir.half==0x4){
			return index-w;
		} else if(dir.half==0x8){
			return index-1;
		}
		return -1;
	}

	uint8_t Graph::getEdges(const int16_t& i, int16_t *edges) const{
		uint8_t cnt=0;
		Coord c=nodes[i].getCoord(w);
		if(!maze->isSetWall(c,c.dir)){
			// $B<+J,<+?H$,M-8z$J%N!<%I$N$H$-(B
			if(i%(2*w-1)<w-1){
				// EAST
				if(c.y<h-1){
					// NE
					if(!maze->isSetWall(nodes[i+w].getCoord(w),Maze::DirFront)){
						edges[cnt++]=i+w;
					}
					// NW
					if(!maze->isSetWall(nodes[i+w-1].getCoord(w),Maze::DirFront)){	
						edges[cnt++]=i+w-1;
					}
				}
				if(c.y>0){
					// SE
					if(!maze->isSetWall(nodes[i-w+1].getCoord(w),Maze::DirFront)){
						edges[cnt++]=i-w+1;
					}
					// SW
					if(!maze->isSetWall(nodes[i-w].getCoord(w),Maze::DirFront)){
						edges[cnt++]=i-w;
					}
				}
				if(c.x<w-2){
					// E
					if(!maze->isSetWall(nodes[i+1].getCoord(w),Maze::DirRight)){
						edges[cnt++]=i+1;
					}
				}
				if(c.x>0){
					// W
					if(!maze->isSetWall(nodes[i-1].getCoord(w),Maze::DirRight)){
						edges[cnt++]=i-1;
					}
				}
			} else {
				// NORTH
				if(c.y<h-1){
					// NE
					if(c.x<w-1){
						if(!maze->isSetWall(nodes[i+w].getCoord(w),Maze::DirRight)){
							edges[cnt++]=i+w;
						}
					}
					// NW
					if(c.x>0){
						if(!maze->isSetWall(nodes[i+w-1].getCoord(w),Maze::DirRight)){	
							edges[cnt++]=i+w-1;
						}
					}
				}
				// SE
				if(c.x<w-1){
					if(!maze->isSetWall(nodes[i-w+1].getCoord(w),Maze::DirRight)){
						edges[cnt++]=i-w+1;
					}
				}
				// SW
				if(c.x>0){
					if(!maze->isSetWall(nodes[i-w].getCoord(w),Maze::DirRight)){
						edges[cnt++]=i-w;
					}
				}
				// S
				if(c.y>0){
					if(!maze->isSetWall(nodes[i-2*w+1].getCoord(w),Maze::DirFront)){
						edges[cnt++]=i-2*w+1;
					}
				}
				// N
				if(c.y<h-2){
					if(!maze->isSetWall(nodes[i+2*w-1].getCoord(w),Maze::DirFront)){
						edges[cnt++]=i+2*w-1;
					}
				}
			}
		}
		return cnt;
	}

	void Graph::loadEmpty(uint8_t w, uint8_t h){
		std::vector<Node>().swap(nodes);
		std::vector<int16_t>().swap(costs);
		nodes.reserve(2*w*h-w-h);
		costs.reserve(2*w*h-w-h);
		for(int i=0;i<(2*w*h-w-h);i++){
			nodes.push_back(Node(i));
			costs.push_back(-1);
		}
	}

	void Graph::loadMaze(Maze const *_maze){
		w=_maze->getWidth();
		h=_maze->getHeight();
		maze=_maze;
	}

	void Graph::dijkstra(int16_t start,int16_t end,bool isSearching){
		costs[start]=0;

		std::priority_queue<Node *> q;
		q.push(&nodes[start]);

		while(!q.empty()){
			Node *doneNode=q.top();
			q.pop();
			int16_t index=doneNode->getIndex();
			doneNode->setDone();
			if(isSearching||doneNode->isVisited()){
				int16_t edges[6]={};
				int n=getEdges(index,edges);
				for(int i=0;i<n;i++){
					int16_t to=edges[i];
					if(to<0) continue;	// $BL58z2=$5$l$?%(%C%8(B
					int cost=costs[index]+1;

					int16_t ang=abs(2*index-doneNode->getFrom()-to);
					if(doneNode!=&nodes[start]){
						if(isInvalid135Deg(doneNode->getFrom(),index,to)){
							// 135[deg]
							cost+=1000;
						} else if(ang==w||ang==w-1){
							// 45[deg]
							cost+=4;
						} else if(ang==1||ang==2*w-1){
							// 90[deg]
							cost+=8;
						}
						if(isV90Deg(doneNode->getFrom(),index,to)){
							cost+=5;
						}
					}

					if(costs[to]==0||cost<costs[to]){
						costs[to]=cost;
						nodes[to].setFrom(index);
					}
				}
			}
		}
	}

	void Graph::astar(int16_t start,int16_t goal,bool isSearching, bool isWeightSearched){
		resetDone();
		//resetFrom();
		Node *goalNode=&nodes[goal];
		Coord c1=goalNode->getCoord(w);

		costs[start]=0;

		std::multiset<int16_t, CompareCost> q(compareCost);
		q.insert(start);

		while(!q.empty()){
			auto itr = q.begin();
			int16_t index=*itr;
			Node *doneNode=getNodePointer(index);
			//if(index==goal){
			//	return;
			//}
			q.erase(itr);
			doneNode->setDone();
			Coord c = doneNode->getCoord(maze->getWidth());
			if(isSearching||maze->isSearchedWall(c, c.dir)){
				int16_t edges[6]={};
				int n=getEdges(index,edges);
				for(int i=0;i<n;i++){
					int16_t to=edges[i];
					if(to<0) continue;
					if(nodes[to].isDone()) continue;
					int cost=costs[index]+1;//+(isWeightSearched&&isSearching)*nodes[to].isVisited();
					Coord c2=nodes[to].getCoord(w);
					if(index!=start){
						int16_t ang=abs(2*index-doneNode->getFrom()-to);
						if(isInvalid135Deg(doneNode->getFrom(),index,to)){
							// 135[deg]
							continue;
						} else if(ang==w||ang==w-1){
							// 45[deg]
							cost+=5;
						} else if(ang==1||ang==2*w-1){
							// 90[deg]
							cost+=9;
						}
						if(isV90Deg(doneNode->getFrom(),index,to)){
							cost+=1;
						}
						/*
						if(abs(to-index)!=1 && abs(to-index)!=2*w-1){
							// avoid diagonal path
							cost+=10;
						}
						*/
					}

					int dist=(abs(c1.x-c2.x)+abs(c1.y-c2.y));
					cost+=dist/16;

					if(costs[to]==0||cost<costs[to]){
						costs[to]=cost;
						nodes[to].setFrom(index);
						q.insert(to);
					}
				}
			}
			//for(auto it=q.begin(); it!=q.end(); ++it){
			//	std::cout<<(*it)<<"("<<costs[*it]<<")"<<",";
			//}
			//std::cout<<std::endl;
		}
	}

	void Graph::getRoute(int16_t start,int16_t end,Route &route) {
		int16_t cursor=end;
		int16_t diff=0;

		while(route.size()<1000&&cursor>=0&&cursor!=start){
			route.push_back(cursor);
			if(!nodes[cursor].isDone()||diff!=cursor-nodes[cursor].getFrom()){
				diff=cursor-nodes[cursor].getFrom();
			}
			cursor=nodes[cursor].getFrom();
		}
        if(route.size()>=1000){
            // falling back with dijkstra
            resetCost();
            dijkstra(start, end, false);
            route.clear();
            while(route.size()<1000&&cursor>=0&&cursor!=start){
                route.push_back(cursor);
                if(!nodes[cursor].isDone()||diff!=cursor-nodes[cursor].getFrom()){
                    diff=cursor-nodes[cursor].getFrom();
                }
                cursor=nodes[cursor].getFrom();
            }
        }
		route.push_back(start);
	}

	bool Graph::isInvalid135Deg(int16_t pre,int16_t now,int16_t next){
		int16_t edges[2];
		edges[0]=now-pre;
		edges[1]=next-now;
		if(edges[0]==edges[1]) return false;
		for(uint8_t i=0;i<2;i++){
			int8_t sgn=(edges[i]>0)-(edges[i]<0);
			if(abs(edges[i])==2*w-1&&(edges[!i]==-sgn*w||edges[!i]==-sgn*(w-1))) return true;
			if(abs(edges[i])==1&&(edges[!i]==-sgn*w||edges[!i]==sgn*(w-1))) return true;
		}
		return false;
	}
	bool Graph::isV90Deg(int16_t pre,int16_t now,int16_t next){
		int16_t edges[2];
		edges[0]=now-pre;
		edges[1]=next-now;
		if(abs(edges[0])==w-1 && abs(edges[1])==w) return true;
		if(abs(edges[1])==w-1 && abs(edges[0])==w) return true;
		return false;
	}
}
