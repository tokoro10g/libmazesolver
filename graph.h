#pragma once

#include "maze.h"
#include "helper.h"
#include <vector>
//#include <queue>
//#include <list>
//#include <utility>
#include <functional>

namespace MazeSolver{

	typedef std::vector<int> Route;

	class Node{
		public:
			Node():index(-1),done(false),visited(false),from(-1){}
			Node(int16_t _index):index(_index),done(false),visited(false),from(-1){}
			~Node();

			int16_t getIndex() const { return index; }
			inline Coord getCoord(const uint8_t w) const{
				if(index%(2*w-1)<w-1){
					// EAST
					return Coord(index%(2*w-1),index/(2*w-1),0x2);
				} else {
					// NORTH
					return Coord(index%(2*w-1)-w+1,index/(2*w-1),0x1);
				}
			}
			void reset(){
				done=false; visited=false; from=-1;
			}
			bool isDone() const { return done; }
			void setDone() { done=true; }
			void resetDone(){ done=false; }
			bool isVisited() const { return visited; }
			void setVisited() { visited=true; }
			int16_t getFrom() const { return from; }
			void setFrom(int16_t _from){ from=_from; }
		public:
			static int16_t generateIndex(const Coord &c,Direction dir,uint8_t w);
		private:
			int16_t index;
			bool done;
			bool visited;
			int16_t from;
	};

	class Graph{
		public:
			Graph():compareCost(&costs){}
			~Graph(){}

			uint8_t getWidth() const { return w; }
			uint8_t getHeight() const { return h; }
			void setWidth(uint8_t _w) { w=_w; }
			void setHeight(uint8_t _h) { h=_h; }

			void reset(){
				for(Node n : nodes){
					n.reset();
				}
				resetCost();
			}

			std::vector<Node> const &getNodes() const { return nodes; }
			inline Node const &getNode(int16_t index) const { return nodes[index]; }
			inline Node *getNodePointer(int16_t index) { return &nodes[index]; }

			uint8_t getEdges(const int16_t& index, int16_t *edges) const;

			void loadMaze(Maze const *_maze);
			void loadEmpty(uint8_t w,uint8_t h);
			inline void resetCost(){ std::fill(costs.begin(), costs.end(), 0); }
			inline void resetDone(){ for(auto it=nodes.begin(); it!=nodes.end(); it++) (*it).resetDone(); }
			inline void resetFrom(){ for(auto it=nodes.begin(); it!=nodes.end(); it++) (*it).setFrom(-1); }
			void dijkstra(int16_t start,int16_t end,bool isSearching);
			void astar(int16_t start,int16_t end,bool isSearching,bool isWeightSearched);
			void getRoute(int16_t start,int16_t end,Route &route);
		private:
			bool isInvalid135Deg(int16_t pre,int16_t now,int16_t next);
			bool isV90Deg(int16_t pre,int16_t now,int16_t next);

			struct CompareCost {
				CompareCost(std::vector<int16_t> *_costs) : costs(_costs) {}
				bool operator ()(const int16_t& i,const int16_t& j) const{
					return (*costs)[i]<(*costs)[j]; // greater-than comparison
				}
				std::vector<int16_t> *costs;
			};

			CompareCost compareCost;

		private:
			Maze const* maze;
			uint8_t w;
			uint8_t h;
			std::vector<int16_t> costs;
			std::vector<Node> nodes;
	};
}
