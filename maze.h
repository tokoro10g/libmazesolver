#pragma once

#include <vector>
#include <cstdint>

namespace MazeSolver{

	union __attribute__ ((__packed__)) Direction {
		uint8_t half:4;
		struct __attribute__ ((__packed__)) {
			unsigned NORTH:1;
			unsigned EAST:1;
			unsigned SOUTH:1;
			unsigned WEST:1;
		} bits;

		const Direction operator- (const Direction& rhs) const {
			Direction ret;
			if(bits.NORTH){
				return rhs;
			} else if(bits.EAST) {
				if(rhs.bits.NORTH){ ret.half=0x8; return ret;}
				ret.half = rhs.half >> 1; return ret;
			} else if(bits.SOUTH) {
				if(rhs.bits.NORTH || rhs.bits.EAST){ ret.half = rhs.half << 2; return ret; }
				ret.half = rhs.half >> 2; return ret;
			} else if(bits.WEST) {
				if(rhs.bits.WEST){ ret.half = 1; return ret; }
				ret.half = rhs.half << 1; return ret;
			} else {
				return rhs;
			}
		}
		const Direction operator+ (const Direction& rhs) const {
			Direction ret;
			if(bits.NORTH){
				return rhs;
			} else if(bits.EAST) {
				if(rhs.bits.WEST){ ret.half=0x1; return ret;}
				ret.half = rhs.half << 1; return ret;
			} else if(bits.SOUTH) {
				if(rhs.bits.NORTH || rhs.bits.EAST){ ret.half = rhs.half << 2; return ret; }
				ret.half = rhs.half >> 2; return ret;
			} else if(bits.WEST) {
				if(rhs.bits.NORTH){ ret.half = 8; return ret; }
				ret.half = rhs.half >> 1; return ret;
			} else {
				return rhs;
			}
		}
	};

	union __attribute__ ((__packed__)) CellData {
		uint8_t half:8;
		struct __attribute__ ((__packed__)) {
			unsigned NORTH:1;
			unsigned EAST:1;
			unsigned SOUTH:1;
			unsigned WEST:1;
			unsigned CHK_NORTH:1;
			unsigned CHK_EAST:1;
			unsigned CHK_SOUTH:1;
			unsigned CHK_WEST:1;
		} bits;
	};

	typedef std::vector<CellData> MazeData;

	struct __attribute__ ((__packed__)) Coord {
		uint8_t x;
		uint8_t y;
		Direction dir;
		Coord():x(0),y(0),dir({0}){}
		Coord(uint8_t _x,uint8_t _y,Direction _dir):x(_x),y(_y),dir(_dir){}
		Coord(uint8_t _x,uint8_t _y,uint8_t _dir):x(_x),y(_y){
			dir.half=_dir;
		}
	};

	class Maze{
		private:
			MazeData data;
			uint8_t w;
			uint8_t h;
			uint8_t type;
			uint8_t gx,gy;
			Direction gd;
		public:
			static const Direction DirFront;
			static const Direction DirRight;
			static const Direction DirBack;
			static const Direction DirLeft;
			static const Direction DirNorth;
			static const Direction DirEast;
			static const Direction DirSouth;
			static const Direction DirWest;
		public:
			Maze():w(0),h(0),type(0),gx(0),gy(0){
				gd.half=0x1;
			}
			Maze(uint8_t _w,uint8_t _h):w(_w),h(_h),type(0),gx(0),gy(0){
				gd.half=0x1;
			}
			Maze(const Maze& m):data(m.data),w(m.w),h(m.h),type(m.type),gx(m.gx),gy(m.gy){
				gd.half=m.gd.half;
			}
			~Maze(){ data.clear(); }
			uint8_t getWidth() const{ return w; }
			uint8_t getHeight() const{ return h; }
			void setType(uint8_t _type){ type=_type; }
			uint8_t getGoalX() const{ return gx; }
			uint8_t getGoalY() const{ return gy; }
			Direction getGoalDir() const { return gd; }
			void setGoal(uint8_t _gx,uint8_t _gy){ gx=_gx;gy=_gy; }
			void setGoalDir(Direction d){ gd.half=d.half; }

			void resize(uint8_t _w,uint8_t _h);

			void replace(const Maze& other);

			CellData getCellData(uint8_t x, uint8_t y) const{
				return data[y*w+x];
			}

			uint16_t getGoalNodeIndex() const{
				return coordToIndex(Coord(gx,gy,(gd.half&0x0F)));
			}

			uint16_t coordToIndex(Coord c) const{
				if(c.dir.half==DirNorth.half){
					return c.y*(2*w-1)+c.x+w-1;
				} else if(c.dir.half==DirEast.half){
					return c.y*(2*w-1)+c.x;
				} else if(c.dir.half==DirSouth.half){
					return c.y*(2*w-1)+c.x-w;
				} else if(c.dir.half==DirWest.half){
					return c.y*(2*w-1)+c.x-1;
				}
				return 65535;
			}

			void setCell(uint16_t index,CellData v){
				data.at(index)=v;
			}
			CellData getCell(uint16_t index) const{
				return data[index];
			}

			void setWall(const Coord &c,Direction dir){
				data.at(c.y*w+c.x).half|=dir.half;

				if(dir.bits.SOUTH&&c.y!=0){
					data.at((c.y-1)*w+c.x).bits.NORTH=1;
				}
				if(dir.bits.EAST&&c.x!=w-1){
					data.at(c.y*w+c.x+1).bits.WEST=1;
				}
				if(dir.bits.NORTH&&c.y!=w-1){
					data.at((c.y+1)*w+c.x).bits.SOUTH=1;
				}
				if(dir.bits.WEST&&c.x!=0){
					data.at(c.y*w+c.x-1).bits.EAST=1;
				}
			}

			void setChkWall(const Coord &c,Direction dir){
				data.at(c.y*w+c.x).half|=(dir.half<<4);

				if(dir.bits.SOUTH&&c.y!=0){
					data.at((c.y-1)*w+c.x).bits.CHK_NORTH=1;
				}
				if(dir.bits.EAST&&c.x!=w-1){
					data.at(c.y*w+c.x+1).bits.CHK_WEST=1;
				}
				if(dir.bits.NORTH&&c.y!=w-1){
					data.at((c.y+1)*w+c.x).bits.CHK_SOUTH=1;
				}
				if(dir.bits.WEST&&c.x!=0){
					data.at(c.y*w+c.x-1).bits.CHK_EAST=1;
				}
			}

			void unsetWall(const Coord &c,Direction dir){
				data.at(c.y*w+c.x).half&=~(dir.half);

				if(dir.bits.SOUTH&&c.y!=0){
					data.at((c.y-1)*w+c.x).bits.NORTH=0;
				}
				else if(dir.bits.EAST&&c.x!=w-1){
					data.at(c.y*w+c.x+1).bits.WEST=0;
				}
				else if(dir.bits.NORTH&&c.y!=w-1){
					data.at((c.y+1)*w+c.x).bits.SOUTH=0;
				}
				else if(dir.bits.WEST&&c.x!=0){
					data.at(c.y*w+c.x-1).bits.EAST=0;
				}
			}

			void unsetChkWall(const Coord &c,Direction dir){
				data.at(c.y*w+c.x).half&=~(dir.half<<4);

				if(dir.bits.SOUTH&&c.y!=0){
					data.at((c.y-1)*w+c.x).bits.CHK_NORTH=0;
				}
				else if(dir.bits.EAST&&c.x!=w-1){
					data.at(c.y*w+c.x+1).bits.CHK_WEST=0;
				}
				else if(dir.bits.NORTH&&c.y!=w-1){
					data.at((c.y+1)*w+c.x).bits.CHK_SOUTH=0;
				}
				else if(dir.bits.WEST&&c.x!=0){
					data.at(c.y*w+c.x-1).bits.CHK_EAST=0;
				}
			}

			void toggleWall(const Coord &c, Direction dir){
				if(isSetWall(c,dir)){
					unsetWall(c,dir);
				} else {
					setWall(c,dir);
				}
			}

			void toggleChkWall(const Coord &c, Direction dir){
				if(isSearchedWall(c,dir)){
					unsetChkWall(c,dir);
				} else {
					setChkWall(c,dir);
				}
			}

			bool isSetWall(const Coord &c,Direction dir) const{
				return data.at(c.y*w+c.x).half&dir.half;
			}

			bool isSetWall(const uint16_t index,Direction dir) const{
				return data.at(index).half&dir.half;
			}

			bool isSearchedWall(const Coord &c,Direction dir) const{
				return data.at(c.y*w+c.x).half&(dir.half<<4);
			}
			bool isSearchedWall(const uint16_t index,Direction dir) const{
				return data.at(index).half&(dir.half<<4);
			}

			bool isSearchedCell(const Coord &c) const{
				return (data.at(c.y*w+c.x).half&0xF0)==0xF0;
			}
			bool isSearchedCell(const uint16_t index) const{
				return (data.at(index).half&0xF0)==0xF0;
			}
	};
}
