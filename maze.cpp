//#include <iostream>
//#include "vector"
#include "maze.h"

namespace MazeSolver{

	const Direction Maze::DirFront={0x1};
	const Direction Maze::DirRight={0x2};
	const Direction Maze::DirBack={0x4};
	const Direction Maze::DirLeft={0x8};
	const Direction Maze::DirNorth={0x1};
	const Direction Maze::DirEast={0x2};
	const Direction Maze::DirSouth={0x4};
	const Direction Maze::DirWest={0x8};

	void Maze::resize(uint8_t _w,uint8_t _h){
		w=_w; h=_h;
		data.resize(w*h);
	}

	void Maze::replace(const Maze& other){
		this->w = other.w;
		this->h = other.h;
		this->type = other.type;
		this->gx = other.gx;
		this->gy = other.gy;
		this->gd = other.gd;
		this->data.clear();
		for(auto c: other.data){
			this->data.push_back(c);
		}
	}
}
