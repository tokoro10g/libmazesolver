SHELL = /bin/sh
COMPILE_OPTS  = -MMD -std=gnu++11 -O3 -g3 -Wall
DEPS = $(patsubst %.cpp,%.d,$(wildcard *.cpp))

CC      = g++
CXX     = $(CC)
AS      = $(CC)
AR      = ar
CFLAGS  = $(COMPILE_OPTS)
CXXFLAGS= $(COMPILE_OPTS) -include stdint.h

.PHONY: all $(SUBDIRS)

all: libmazesolver.a

libmazesolver.a: $(patsubst %.cpp,%.o,$(wildcard *.cpp))
	$(AR) rcs libmazesolver.a $^

clean:
	rm -rf *.o *.d *.a

-include $(DEPS)
