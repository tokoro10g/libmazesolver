#pragma once
#include "maze.h"

namespace MazeSolver {
	class WallSource{
		public:
			WallSource(){}
			virtual ~WallSource(){};
			virtual bool isSetWall(Coord c, Direction d, Direction rd)=0;
	};
}
