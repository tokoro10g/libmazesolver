#pragma once
#include <queue>

template<typename T, typename S, typename C>
class iterable_priority_queue: public std::priority_queue<T,S,C>{
	public:
		iterable_priority_queue(C c):std::priority_queue<T,S,C>(c){ }
		typename S::iterator begin(){ return this->iterable_priority_queue::c.begin(); }
		typename S::iterator end(){ return this->iterable_priority_queue::c.end(); }
		typename S::const_iterator begin() const{ return this->iterable_priority_queue::c.begin(); }
		typename S::const_iterator end() const{ return this->iterable_priority_queue::c.end(); }
};
