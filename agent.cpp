#include "agent.h"
#include <algorithm>
//#include <iostream>

namespace MazeSolver{
	bool Agent::searchMaze(int16_t start,int16_t goal,bool isSearching){
		index=start;
		while(index!=goal){
			if(stepMaze(goal,isSearching)<0){
				// 到達不能
				return false;
			}
		}
		return true;
	}

	void Agent::getShortestRoute(int16_t start, int16_t goal, Route& route){
		graph->resetCost();
		graph->astar(start,goal,false,false);
		Route().swap(route);
		graph->getRoute(start,goal,route);
		std::reverse(route.begin(),route.end());
	}

	int Agent::stepMaze(int16_t goal,bool isSearching){
		static uint16_t iter=1;
		static uint8_t recur=0;

		// avoids stack overflow
		if(recur>4){ recur = 0; return -1; }

		graph->getNodePointer(index)->setVisited();

		if(index==goal){
			recur=0;
			return -1;
		}
		Coord ggc=graph->getNode(goal).getCoord(maze->getWidth());
		if(maze->isSetWall(ggc,ggc.dir)){
			recur=0;
			return -1;
		}

		if(rerouteFlag){
			rerouteFlag=false;
			iter=1;
			graph->resetCost();
			graph->astar(goal,index,isSearching,isSearching);
			Route().swap(route);
			graph->getRoute(goal,index,route);
			if(route.size()==2){
				Direction d=nextDir(route[0],route[1],true);
				if(d.half==0){
					recur=0;
					return -1;
				}
			}
		}
		Direction newDir=nextDir(index,route[iter],true);
		if(isSearching){
			c=graph->getNode(index).getCoord(maze->getWidth());
			if(isPullBack(index,route[iter],newDir)){
				static const unsigned char rev[8]={4,8,0,1,0,0,0,2};
				dir.half=rev[dir.half-1];
				rerouteFlag=true;
				if(recur>0) recur--;
				return index;
			}
			senseAll();
			if(sense(newDir)){
				// 壁があって進めない
				rerouteFlag=true;
				recur++;
				return stepMaze(goal,isSearching);
			}
		}
		dir=newDir;
		if(iter>=route.size()){
			recur=0;
			return -1;
		}
		index=route[iter];
		iter++;

		if(isInGoalCell(index,goal)){
			if(goal==maze->getGoalNodeIndex()){
				Coord gc=graph->getNode(index).getCoord(maze->getWidth());
				if(gc.x!=maze->getGoalX()){
					gc.dir.half=Maze::DirWest.half;
				}
				if(gc.y!=maze->getGoalY()){
					gc.dir.half=Maze::DirSouth.half;
				}
				maze->setGoalDir(gc.dir);
			}
			rerouteFlag=true;
			iter=1;
		}
		if(recur>0) recur--;
		return index;
	}

	int16_t Agent::searchCandidateNode(int16_t start){
		//Coord frontCoord = c;
		//int8_t x, y;
		//if(dir.half == Maze::DirNorth.half) {
		//	y = (int8_t)frontCoord.y + 1;
		//} else if(dir.half == Maze::DirEast.half) {
		//	x = (int8_t)frontCoord.x + 1;
		//} else if(dir.half == Maze::DirSouth.half) {
		//	y = (int8_t)frontCoord.y - 1;
		//} else if(dir.half == Maze::DirWest.half) {
		//	x = (int8_t)frontCoord.x - 1;
		//}
		//if(x>=0 && x<maze->getWidth() && y>=0 && y<maze->getHeight()){
		//	frontCoord.x = x; frontCoord.y = y;
		//	if(!maze->isSearchedWall(frontCoord, dir) && !maze->isSetWall(frontCoord, dir)){
		//		if(dir.bits.SOUTH&&c.y!=0 &&
		//			dir.bits.EAST&&c.x!=maze->getWidth()-1 &&
		//			dir.bits.NORTH&&c.y!=maze->getWidth()-1 &&
		//			dir.bits.WEST&&c.x!=0 &&
		//			index != maze->coordToIndex(frontCoord)){
		//			return maze->coordToIndex(frontCoord);
		//		}
		//	}
		//}

		graph->resetCost();
		graph->astar(maze->getGoalNodeIndex(),start,true,true);
		Route r;
		graph->getRoute(maze->getGoalNodeIndex(),start,r);
		rerouteFlag=true;
		for(auto it=r.begin()+1;it!=r.end()-1;it++){
			Node* np = graph->getNodePointer(*it);
			Coord nc = np->getCoord(maze->getWidth());
			if(index != (*it) && !np->isVisited() && !maze->isSearchedWall(nc, nc.dir) && !maze->isSetWall(nc, nc.dir)){
				return (*it);
			}
		}
		return -1;
	}

	Direction Agent::nextDir(int16_t from,int16_t to,bool isSearching){
		Direction d;
		d.half=0;
		uint8_t w=maze->getWidth();
		int16_t diff=to-from;
		if(diff==0){
			return dir;
		}
		if(diff==2*w-1){
			return Maze::DirNorth;
		} else if(diff==-2*w+1){
			return Maze::DirSouth;
		} else if(diff==1){
			return Maze::DirEast;
		} else if(diff==-1){
			return Maze::DirWest;
		} else {
			if(isSearching){
				// 4方向のみ考慮
				if(diff==w){
					// NE
					if(from%(2*w-1)<w-1){
						// EAST
						d.bits.NORTH=1;
					} else {
						// NORTH
						d.bits.EAST=1;
					}
				} else if(diff==w-1){
					// NW
					if(from%(2*w-1)<w-1){
						// EAST
						d.bits.NORTH=1;
					} else {
						// NORTH
						d.bits.WEST=1;
					}
				} else if(diff==-w+1){
					// SE
					if(from%(2*w-1)<w-1){
						// EAST
						d.bits.SOUTH=1;
					} else {
						// NORTH
						d.bits.EAST=1;
					}
				} else if(diff==-w){
					// SW
					if(from%(2*w-1)<w-1){
						// EAST
						d.bits.SOUTH=1;
					} else {
						// NORTH
						d.bits.WEST=1;
					}
				}
			} else {
				// 8方向考慮
				if(diff==w){
					// NE
					d.bits.NORTH=1;
					d.bits.EAST=1;
				} else if(diff==w-1){
					// NW
					d.bits.NORTH=1;
					d.bits.WEST=1;
				} else if(diff==-w+1){
					// SE
					d.bits.SOUTH=1;
					d.bits.EAST=1;
				} else if(diff==-w){
					// SW
					d.bits.SOUTH=1;
					d.bits.WEST=1;
				}
			}
		}
		return d;
	}

	bool Agent::isPullBack(int16_t from,int16_t to,Direction newDir) const{
		return isPullBack(from,to,dir,newDir);
	}
	bool Agent::isPullBack(int16_t from,int16_t to,Direction _dir,Direction newDir) const{
		if(newDir.half==_dir.half){
			return false;
		}
		uint8_t w=maze->getWidth();
		if(from%(2*w-1)<w-1){
			// EAST
			if(_dir.half==0x2){
				if(newDir.half==0x8) return true;
				if(newDir.half==0x1&&to!=from+w){
					return true;
				}
				if(newDir.half==0x4&&to!=from-w+1){
					return true;
				}
			} else if(_dir.half==0x8) {
				if(newDir.half==0x2) return true;
				if(newDir.half==0x1&&to!=from+w-1){
					return true;
				}
				if(newDir.half==0x4&&to!=from-w){
					return true;
				}
			}
		} else {
			// NORTH
			if(_dir.half==0x1){
				if(newDir.half==0x4) return true;
				if(newDir.half==0x2&&to!=from+w){
					return true;
				}
				if(newDir.half==0x8&&to!=from+w-1){
					return true;
				}
			} else if(_dir.half==0x4){
				if(newDir.half==0x1) return true;
				if(newDir.half==0x2&&to!=from-w+1){
					return true;
				}
				if(newDir.half==0x8&&to!=from-w){
					return true;
				}
			}
		}
		return false;
	}

	Direction Agent::getRelativeDir(Direction d) const{
		if(dir.bits.NORTH){
			return d;
		} else if(dir.bits.EAST) {
			if(d.bits.NORTH){ d.half=0x8; return d;}
			d.half >>= 1; return d;
		} else if(dir.bits.SOUTH) {
			if(d.bits.NORTH || d.bits.EAST){ d.half <<= 2; return d; }
			d.half >>= 2; return d;
		} else if(dir.bits.WEST) {
			if(d.bits.WEST){ d.half = 1; return d; }
			d.half <<= 1; return d;
		} else {
			return d;
		}
	}

	bool Agent::sense(Direction newDir) const{
		return sensorData.half&newDir.half;
	}

	bool Agent::senseFront() const{
		uint8_t w=maze->getWidth();
		uint8_t h=maze->getHeight();
		if((dir.half==0x1&&c.y==h-1)
				|| (dir.half==0x2&&c.x==w-1)
				|| (dir.half==0x4&&c.y==0)
				|| (dir.half==0x8&&c.x==0)){
			return true;
		}
		if(maze->isSearchedWall(c,dir)){
			return maze->isSetWall(c,dir);
		}
		maze->setChkWall(c,dir);
		//graph->getNodePointer(Node::generateIndex(c,dir,w))->setVisited();
		if(wallSource->isSetWall(c,dir,Maze::DirFront)){
			maze->setWall(c,dir);
			return true;
		} else {
			return false;
		}
	}

	bool Agent::senseRight() const{
		Direction d;
		if(dir.half==0x8){
			d.half=0x1;
		} else {
			d.half=dir.half<<1;
		}
		uint8_t w=maze->getWidth();
		uint8_t h=maze->getHeight();
		if((d.half==0x1&&c.y==h-1)
				|| (d.half==0x2&&c.x==w-1)
				|| (d.half==0x4&&c.y==0)
				|| (d.half==0x8&&c.x==0)){
			return true;
		}
		if(maze->isSearchedWall(c,d)){
			return maze->isSetWall(c,d);
		}
		maze->setChkWall(c,d);
		//graph->getNodePointer(Node::generateIndex(c,d,w))->setVisited();
		if(wallSource->isSetWall(c,d,Maze::DirRight)){
			maze->setWall(c,d);
			return true;
		} else {
			return false;
		}
	}

	bool Agent::senseLeft() const{
		Direction d;
		if(dir.half==0x1){
			d.half=0x8;
		} else {
			d.half=dir.half>>1;
		}
		uint8_t w=maze->getWidth();
		uint8_t h=maze->getHeight();
		if((d.half==0x1&&c.y==h-1)
				|| (d.half==0x2&&c.x==w-1)
				|| (d.half==0x4&&c.y==0)
				|| (d.half==0x8&&c.x==0)){
			return true;
		}
		if(maze->isSearchedWall(c,d)){
			return maze->isSetWall(c,d);
		}
		maze->setChkWall(c,d);
		//graph->getNodePointer(Node::generateIndex(c,d,w))->setVisited();
		if(wallSource->isSetWall(c,d,Maze::DirLeft)){
			maze->setWall(c,d);
			return true;
		} else {
			return false;
		}
	}

	void Agent::senseAll(){
		sensorData.half=0;
		if(dir.bits.SOUTH){
			// 南を向いている時(NORTHノードにいるはず)
			// 得た座標の周囲の壁情報を更新
			sensorData.bits.SOUTH=senseFront()?1:0;
			sensorData.bits.WEST=senseRight()?1:0;
			sensorData.bits.EAST=senseLeft()?1:0;
		} else if(dir.bits.NORTH){
			// 北を向いている時(NORTHノードにいるはず)
			// 得た座標.y+1の周囲の壁情報を更新
			c.y++;
			sensorData.bits.NORTH=senseFront()?1:0;
			sensorData.bits.EAST=senseRight()?1:0;
			sensorData.bits.WEST=senseLeft()?1:0;
		} else if(dir.bits.EAST){
			// 東を向いている時(EASTノードにいるはず)
			// 得た座標.x+1の周囲の壁情報を更新
			c.x++;
			sensorData.bits.EAST=senseFront()?1:0;
			sensorData.bits.SOUTH=senseRight()?1:0;
			sensorData.bits.NORTH=senseLeft()?1:0;
		} else if(dir.bits.WEST){
			// 西を向いている時(EASTノードにいるはず)
			// 得た座標の周囲の壁情報を更新
			sensorData.bits.WEST=senseFront()?1:0;
			sensorData.bits.NORTH=senseRight()?1:0;
			sensorData.bits.SOUTH=senseLeft()?1:0;
		}
	}

	bool Agent::isInGoalCell(int16_t index, int16_t goal) const{
		Coord gc;
		if(goal==maze->getGoalNodeIndex()){
			gc.x=maze->getGoalX();
			gc.y=maze->getGoalY();
			gc.dir.half=0x1;
		} else {
			gc=graph->getNodePointer(goal)->getCoord(maze->getWidth());
		}
		while(gc.dir.half!=0x0){
			if(maze->coordToIndex(gc)==index){
				return true;
			}
			gc.dir.half<<=1;
		}
		return false;
	}

	uint8_t Agent::getCurrentEdges(int16_t *edges) const{
		return graph->getEdges(index, edges);
	}
}
